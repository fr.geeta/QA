package Lesson7;

import java.util.Random;

public class LITSUser implements IUser {
    private String name;
    private String password;
    private int ID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void registration(String name, String password) {
        System.out.println("LITS User was registered");
    }

    public int generateID() {
        return new Random().nextInt(SECRET);
    }
}
