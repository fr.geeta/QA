package Lesson7;

public class Test {
    public static void main(String[] args) {
        IUser iUser = new LITSUser();
        iUser.registration("name", "pass");

        IUser iUser2 = new AutoClass();
        iUser2.registration("name", "pass");

        IUser iUser3 = new SuperAutoUser();
        iUser3.registration("da", "pass");
    }
}
