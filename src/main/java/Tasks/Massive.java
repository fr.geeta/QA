package Tasks;

import java.util.Arrays;

public class Massive {
    public static void main(String[] args) {
        {
            int[] arr = {1, 12, 25, 45, 78, 29};
            for (int i = 0; arr.length / 2 > i; i++) {
                int tmp = arr[i];
                arr[i] = arr[arr.length - i - 1];
                arr[arr.length - i - 1] = arr[i];
            }
            System.out.println(Arrays.toString(arr));
        }
    }}
