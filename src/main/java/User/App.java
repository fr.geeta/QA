package User;

/**
 * Created by sovaj on 20.02.2018.
 */
public class App {
    public static void main(String[] args) {
        User user = new StudentsUser("David" , 20,true);
        System.out.println(user);
        user.setAge(18);
        System.out.println(user.getAge());
        user.setName("Joe");
        System.out.println(user.getName());

        StudentsUser studentsUser = new StudentsUser("Jack", 34, true);
        studentsUser.setAge(34);
        System.out.println(studentsUser.getAge());
        System.out.println(studentsUser);

        studentsUser.generateID();
        System.out.println(studentsUser.getId());
    }
}
