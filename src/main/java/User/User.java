package User;

public abstract class User implements IUser{
    private String name;
    private int age;
    private boolean male;

    private String login;
    private String password;

    public String toString() {
        return "info - " + name + age + male;
    }

    User(String name, int age, boolean male) {

        this.name=name;
        this.age=age;
        this.male=male;
    }

    int getAge() {
        return age;
    }

    void setAge(int age) {
        this.age = age;
    }

    String getName() {
        return name;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    void setName(String name) {
        this.name = name;

    }

    abstract void print();
}
